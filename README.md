# Compile Code Dynamic on Runtime

The new scripting APIs enable .NET applications to instatiate a C# engine and execute code snippets against host-supplied objects.

https://github.com/dotnet/roslyn/wiki/Scripting-API-Samples
