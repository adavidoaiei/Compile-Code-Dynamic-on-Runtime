﻿using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Threading.Tasks;

namespace EvalCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            program.Eval1();
            program.Eval2();
            program.Eval3();

            Console.ReadKey();
        }

        public async void Eval1() {
            try
            {
                Console.WriteLine(await CSharpScript.EvaluateAsync("2+2"));
            }
            catch (CompilationErrorException e)
            {
                Console.WriteLine(string.Join(Environment.NewLine, e.Diagnostics));
            }
        }

        public async void Eval2()
        {
            var result = await CSharpScript.EvaluateAsync("Sqrt(2)",
                 ScriptOptions.Default.WithImports("System.Math"));

            Console.Write(result);
        }

        public async void Eval3()
        {
            var script = CSharpScript.Create<int>("int x = 1;")
                                    .ContinueWith("int y = 2;")
                                    .ContinueWith("x + y");

            Console.WriteLine("\nResult: " + Task.Run(() => script.RunAsync()).Result.ReturnValue);
        }
    }
}
